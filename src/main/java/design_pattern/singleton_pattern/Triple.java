package design_pattern.singleton_pattern;

public class Triple {
    private int id;

    private static Triple[] triple = new Triple[]{
            new Triple(0),
            new Triple(1),
            new Triple(2)
    };

    private Triple(int id) {
        this.id = id;
    }

    public static Triple getInstance(int id) {
        if (id > 3)throw new IllegalArgumentException("idは３までです。");
        
        return triple[id];
    }
}
