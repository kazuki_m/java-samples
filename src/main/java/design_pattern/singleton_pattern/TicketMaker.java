package design_pattern.singleton_pattern;

public class TicketMaker {

    private static TicketMaker ticketMaker = new TicketMaker();
    private int ticket = 1000;

    private TicketMaker(){
        super();
    }

    public static TicketMaker getInstance(){
        return ticketMaker;
    }

    /* synchronized 1つのスレッドからしか呼び出せなくなる
        synchronizedが無いと、複数のスレッドから呼び出された時、同じ値を返す可能性がある。
     */
    public synchronized int getNextTicketNumber(){
        return ticket ++;
    }
}
