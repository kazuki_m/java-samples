package design_pattern.singleton_pattern;

public class Main {

    public static void useSingletonClass(){

        // getInstanceを呼び出した時にSingletonクラスが初期される。
        Singleton obj1 = Singleton.getInstance();
        Singleton obj2 = Singleton.getInstance();

        if (obj1 == obj2) {
            System.out.println("同じインスタンス");
        }

        // newでインスタンス生成することはできなくなる
//        Singleton obj1 = new Singleton();
    }

    public static void useTriple(){
        for (int i = 0; i < 4; i++){
            Triple triple = Triple.getInstance(i);
            System.out.println("id = " + i + "生成");
        }
    }

    public static void main(String[] args){

        useSingletonClass();
        useTriple();
    }




}
