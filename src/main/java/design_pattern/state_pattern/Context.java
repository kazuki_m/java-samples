package design_pattern.state_pattern;

import design_pattern.state_pattern.state.State;

public interface Context {

	public void setClock(int hour);
	public void changeState(State state);
	public void callSecurityCenter(String message);
	public void recordLog(String message);

}
