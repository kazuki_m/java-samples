package design_pattern.state_pattern.state;

import design_pattern.state_pattern.Context;

public class NightState implements State {

	private static NightState singleton = new NightState();

	private NightState(){
		super();
	}

	public static NightState getInstance() {
		return singleton;
	}

	@Override
	public void doClock(Context context, int hour) {
		if(9 <= hour || hour < 17) {
			context.changeState(DayState.getInstance());
		}
	}

	public void doUse(Context context) {
		context.recordLog("金庫使用（夜間）");
	}

	public void doAlarm(Context context) {
		context.callSecurityCenter("非常ベル（夜間）");
	}

	public void doPhone(Context context) {
		context.callSecurityCenter("通常の電話（夜間）");
	}
}
