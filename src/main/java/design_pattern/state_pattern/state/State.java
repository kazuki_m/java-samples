package design_pattern.state_pattern.state;

import design_pattern.state_pattern.Context;

public interface State {

	public void doClock(Context context, int hour);
	public void doUse(Context context);
	public void doAlarm(Context context);
	public void doPhone(Context context);

}
