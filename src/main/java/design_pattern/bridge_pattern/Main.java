package design_pattern.bridge_pattern;

import design_pattern.bridge_pattern.function.CountDisplay;
import design_pattern.bridge_pattern.function.Display;
import design_pattern.bridge_pattern.function.RandomDisplay;
import design_pattern.bridge_pattern.implement.FileDisplayImpl;
import design_pattern.bridge_pattern.implement.StringDisplayImpl;

// 機能のクラス階層と実装のクラス階層の橋渡しを行うことが出来る。
public class Main {

    public static void main(String[] args) {

//        Display d1 = new Display(new StringDisplayImpl("Hello world"));
//        Display d2 = new CountDisplay(new StringDisplayImpl("Hello Japan"));
//        CountDisplay d3 = new CountDisplay(new StringDisplayImpl("Hello Universe"));
        RandomDisplay d4 = new RandomDisplay(new StringDisplayImpl("Hello USA"));
        CountDisplay d5 = new CountDisplay(new FileDisplayImpl("/Users/kazuki/Development/Java/java-samples/src/main/java/design_pattern/bridge_pattern/resource/sample.txt"));
//        d1.display();
//        d2.display();
//        d3.multipleDisplay(5);
        d4.display(5);
        d5.multipleDisplay(3);

    }
}
