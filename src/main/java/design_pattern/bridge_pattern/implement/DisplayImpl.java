package design_pattern.bridge_pattern.implement;

// Implementor 実装クラスの最上位、Abstraction役のAPIを実装するためのメソッド規定
public interface DisplayImpl {

    void rawOpen();

    void rawPrint();

    void rawClose();
}
