package design_pattern.bridge_pattern.implement;

import java.io.*;

// テキストファイルの内容を表示する
public class FileDisplayImpl implements DisplayImpl {

    private String filename;
    private BufferedReader reader;
    private final int MAX_READHEAD_LIMIT = 4096;

    public FileDisplayImpl(String filename) {
        this.filename = filename;
    }

    public void rawOpen() {
        try {
            reader = new BufferedReader(new FileReader(this.filename));
            reader.mark(MAX_READHEAD_LIMIT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(filename);
    }

    public void rawPrint() {
        try {
            String line;
            reader.reset();
            while ((line = reader.readLine()) != null) {
                System.out.println(">" + line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void rawClose() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


