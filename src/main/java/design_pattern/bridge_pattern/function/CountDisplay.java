package design_pattern.bridge_pattern.function;

import design_pattern.bridge_pattern.implement.DisplayImpl;

// RefinesdAbstraction
// Abstractionに機能を追加
public class CountDisplay extends Display {

    public CountDisplay(DisplayImpl impl) {
        super(impl);
    }

    public void multipleDisplay(int times) {
        open();

        for (int i = 0; i < times; i++) {
            print();
        }

        close();
    }
}
