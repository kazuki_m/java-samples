package design_pattern.bridge_pattern.function;

import design_pattern.bridge_pattern.implement.DisplayImpl;

// Abstaraction
// 機能のクラス階層の最上位、基本的な機能だけを記述(open, print, close, display)
public class Display {

    private DisplayImpl impl;

    public Display(DisplayImpl impl) {
        this.impl = impl;
    }

    public void open() {
        this.impl.rawOpen();
    }

    public void print() {
        this.impl.rawPrint();
    }

    public void close() {
        this.impl.rawClose();
    }

    public void display() {
        open();
        print();
        close();
    }
}
