package design_pattern.bridge_pattern.function;

import design_pattern.bridge_pattern.implement.DisplayImpl;

import java.util.Random;

//import java.util.Random;

public class RandomDisplay extends Display {

    private Random random = new Random();

    public RandomDisplay(DisplayImpl impl) {
        super(impl);
    }

    public void display(int times) {
        open();

        for (int i = 0; i < random.nextInt(times); i++) {
            print();
        }

        close();
    }


}
