package design_pattern.template_method;

public class StringDisplay extends AbstractDisplay {
    private String literal;
    private int width;

    public StringDisplay(String literal) {
        this.literal = literal;
        this.width = literal.getBytes().length;
    }

    @Override
    public void open() {
        printLine();
    }

    @Override
    public void print() {
        System.out.println("|" + this.literal + "|");
    }

    @Override
    public void close() {
        printLine();
    }

    private void printLine(){
        System.out.print("+");

        for (int i = 0; i < this.width; i++){
            System.out.print("-");
        }

        System.out.println("+");
    }
}
