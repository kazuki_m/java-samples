package design_pattern.template_method;

public class Main {

    public static void main(String[] args) {

        AbstractDisplay charDisplay = new CharDisplay('H');
        charDisplay.display();

        AbstractDisplay helloWorldDisplay = new StringDisplay("Hello World");
        helloWorldDisplay.display();

        AbstractDisplay hogeDiaplay = new StringDisplay("hoge");
        hogeDiaplay.display();
    }
}
