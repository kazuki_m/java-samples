package design_pattern.template_method;

public class CharDisplay extends AbstractDisplay {
    private  char literal;

    public CharDisplay(char literal){
        this.literal = literal;
    }

    @Override
    public  void open(){
        System.out.print("<<");
    }

    @Override
    public void print(){
        System.out.print(this.literal);
    }

    @Override
    public void close(){
        System.out.print(">>");
    }
}
