package design_pattern.adapter_pattern.inheritance;

public interface Print {

    void printWeak();
    void printStrong();
}
