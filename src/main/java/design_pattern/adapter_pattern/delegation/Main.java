package design_pattern.adapter_pattern.delegation;


import design_pattern.adapter_pattern.inheritance.Print;
import design_pattern.adapter_pattern.inheritance.PrintBanner;

public class Main {

    public static void main(String[] args) {
        Print p = new PrintBanner("Hello");

        p.printWeak();
        p.printStrong();
    }
}
