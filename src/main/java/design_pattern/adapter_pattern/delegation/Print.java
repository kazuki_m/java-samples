package design_pattern.adapter_pattern.delegation;

public abstract class Print {

    public abstract void printWeak();
    public abstract void printStrong();
}
