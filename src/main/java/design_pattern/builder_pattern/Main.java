package design_pattern.builder_pattern;

public class Main {

    public static void main(String[] args){

        if (args[0].contains("text")){
            useTextBuilder();
        }

        if(args[0].contains("html")){
            useHTMLBuilder();
        }


    }

    //TextBuilder起動
    private static void useTextBuilder(){

        Builder textBuilder = new TextBuilder();

        Director director = new Director(textBuilder);

        director.construct();

        String result = textBuilder.toString();
        System.out.println(result);
    }

    //HTMLBuilder起動
    private static void useHTMLBuilder(){
        Builder htmlBuilder = new HTMLBuilder();
        Director director = new Director(htmlBuilder);

        director.construct();
    }
}
