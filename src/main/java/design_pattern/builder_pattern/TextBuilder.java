package design_pattern.builder_pattern;

public class TextBuilder implements Builder {

    private StringBuffer buffer = new StringBuffer();

    @Override
    public void makeTitle(String title){

        buffer.append("===============\n");
        buffer.append("[" + title + "]");
        buffer.append("\n");
    }

    @Override
    public void makeString(String string){

        buffer.append("■" + string + "\n");
        buffer.append("\n");
    }

    @Override
    public void makeItems(String[] items){
        for(String item: items){
            buffer.append("・" + item + "\n");
        }
    }

    @Override
    public void close(){
        buffer.append("===============");
    }

    public String toString(){
        return buffer.toString();
    }

}
