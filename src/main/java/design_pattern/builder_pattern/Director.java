package design_pattern.builder_pattern;

public class Director {

    private Builder builder;

    public Director(Builder builder) {
        this.builder = builder;
    }

    public void construct() {
        this.builder.makeTitle("Greeting");

        this.builder.makeString("朝から昼にかけて");
        this.builder.makeItems(new String[]{"おはよう", "こんにちは\n"});

        this.builder.makeString("夜");
        this.builder.makeItems(new String[]{"こんばんは", "おやすみなさい"});

        this.builder.close();
    }
}
