package design_pattern.strategy_pattern;

public class Hand {

    public static final int ROCK = 0;
    public static final int SCISSORS = 1;
    public static final int PAPER = 2;

    private int handValue;

    public static final Hand[] HAND = new Hand[]{
            new Hand(ROCK),
            new Hand(SCISSORS),
            new Hand(PAPER)
    };

    private static final String[] name = {
            "グー",
            "チョキ",
            "パー"
    };

    private Hand(int handValue) {
        this.handValue = handValue;
    }

    public static Hand getHand(int handValue) {
        return HAND[handValue];
    }

    public boolean isStrongerThan(Hand hand) {
        return fight(hand) == 1;
    }

    public boolean isWeakerThan(Hand hand) {
        return fight(hand) == -1;
    }

    private int fight(Hand hand) {

        if (this == hand) return 0;
        if ((this.handValue + 1) % 3 == hand.handValue) return 1;

        return -1;
    }

    @Override
    public String toString() {
        return name[handValue];
    }
}
