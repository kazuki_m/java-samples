package design_pattern.strategy_pattern;

public class Player {

    private Strategy strategy;
    private String name;
    private int winCount;
    private int loseCount;
    private int gameCount;

    public Player(String name, Strategy strategy){
        this.name = name;
        this.strategy = strategy;
    }

    public Hand nextHand(){
        return  strategy.nextHand();
    }
    public void win(){
        strategy.study(true);
        winCount++;
        gameCount++;
    }

    public void lose(){
        strategy.study(false);
        loseCount++;
        gameCount++;
    }

    public void even(){
        gameCount++;
    }

    public String toString(){
        return "[" + name + ":" + gameCount + "games. " + winCount + "win " + loseCount + " lose" + "]";
    }
}


