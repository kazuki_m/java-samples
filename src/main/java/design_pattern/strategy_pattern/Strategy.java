package design_pattern.strategy_pattern;

public interface Strategy {

    Hand nextHand();
    void study(boolean win);
}
