package design_pattern.iterator_pattern;

// 内部の実装がどうであれ（この場合は配列でもリストでも）、実装と切り離して数え上げることができる。
public class BookShelfIterator implements Iterator {

    private BookShelf bookShelf;
    private int index;

    public BookShelfIterator(BookShelf bookShelf) {
        this.bookShelf = bookShelf;
        this.index = 0;
    }

    @Override
    public boolean hasNext() {
        if (index < bookShelf.getLength()) {
            return true;
        }
        return false;
    }

    @Override
    public Object next(){
        Book book = bookShelf.getBookAt(index);
        index++;
        return book;
    }
}
