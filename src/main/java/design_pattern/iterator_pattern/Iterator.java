package design_pattern.iterator_pattern;

public interface Iterator {

    boolean hasNext();
    Object next();
}
