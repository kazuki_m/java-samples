package design_pattern.iterator_pattern;

public class Main {

    public static void main(String[] args){

        BookShelf bookShelf = new BookShelf();
        bookShelf.appendBooks(new Book("Aroud the World in 80 Days"));
        bookShelf.appendBooks(new Book("Bible"));
        bookShelf.appendBooks(new Book("Cinderella"));
        bookShelf.appendBooks(new Book("Daddy-Long-Legs"));

        Iterator it = bookShelf.iterator();

        while (it.hasNext()){
            Book book = (Book) it.next();
            System.out.println(book.getName());
        }
    }
}
