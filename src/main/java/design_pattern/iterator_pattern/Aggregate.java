package design_pattern.iterator_pattern;


public interface Aggregate {

    public abstract Iterator iterator();
}
