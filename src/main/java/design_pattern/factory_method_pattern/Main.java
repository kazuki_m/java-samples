package design_pattern.factory_method_pattern;

import design_pattern.factory_method_pattern.framework.Factory;
import design_pattern.factory_method_pattern.framework.Product;
import design_pattern.factory_method_pattern.idcard.IDCardFactory;

public class Main {

    public static void main(String[] args) {

        Factory factory = new IDCardFactory();
        Product card1 = factory.create("hoge", 1);
        Product card2 = factory.create("fuga", 2);
        Product card3 = factory.create("hogehoge", 3);

        card1.use();
        card2.use();
        card3.use();

    }
}
