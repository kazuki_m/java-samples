package design_pattern.factory_method_pattern.idcard;

import design_pattern.factory_method_pattern.framework.Factory;
import design_pattern.factory_method_pattern.framework.Product;

import java.util.ArrayList;
import java.util.List;

/**
 * IDカードを生成するファクトリークラス
 */
public class IDCardFactory extends Factory  {

    private List<String> owners = new ArrayList<String>();


    @Override
    public Product createProduct(String owner, int number) {
        return new IDCard(owner, number);
    }

    @Override
    public void registerProduct(Product product) {
        owners.add(((IDCard)product).getOwner());
    }
}
