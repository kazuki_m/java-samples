package design_pattern.factory_method_pattern.idcard;

import design_pattern.factory_method_pattern.framework.Product;

public class IDCard implements Product {

    private String owner;
    private int number;

    IDCard(String owner, int number){
        this.owner = owner;
        this.number = number;
    }

    @Override
    public void use(){
        System.out.println(this.owner + "(" + this.number + ")" + "のカードを作成");
    }

    public String getOwner() {
        return owner;
    }
}
