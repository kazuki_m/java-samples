package design_pattern.factory_method_pattern.framework;

public abstract class Factory {

    public final Product create(String owner, int number){
         Product p = createProduct(owner, number);
         registerProduct(p);
         return  p;
    }

    // newによる実際のインスタンス生成をインスタンス生成のためのメソッド呼び出しに変えることで具体的なクラス名による束縛からスーパークラスを解放している
    public abstract Product createProduct(String owner, int number);

    public abstract void registerProduct(Product product);
}
