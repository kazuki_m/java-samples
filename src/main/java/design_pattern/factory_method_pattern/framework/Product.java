package design_pattern.factory_method_pattern.framework;

public interface Product {

    void use();
}
