package class_design.loose_coupling;

import java.util.ArrayList;
import java.util.List;

public class Person {

	private String name;

	private List<String> interests;

	public Person(String name) {
		this.name = name;
		this.interests = new ArrayList<String>();
	}

	// これまでの関心ごとを全てメソッドの引数で与えられたもので更新する
	void refleshInterests(List<String> newInterests) {
		this.interests = newInterests;
	}

	// Personの関心ごとをreturnする
	List<String> getAllInterests() {
		return this.interests;
	}

}
