package class_design.loose_coupling;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kazuki
 *
 */
public class LooseCoupling {

	public static void main(String[] args) {

		Person p = new Person("HOGEHOGE FUGA");

		List<String> newIntersts = new ArrayList<>();
		newIntersts.add("テレビ制作");
		newIntersts.add("野球");
		newIntersts.add("toto BIG");

		p.refleshInterests(newIntersts);

		List<String> interests = p.getAllInterests();
		System.out.println(interests);
	}
}
