package class_design.template_method_state_pattern.base_model;

public interface Phase {

	void work();
}
