package class_design.template_method_state_pattern.base_model;

public interface Employee {

	public void set();
	public void takeTrain();
	public void work();
}
