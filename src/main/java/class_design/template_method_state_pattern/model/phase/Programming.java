package class_design.template_method_state_pattern.model.phase;

import class_design.template_method_state_pattern.base_model.Phase;

public class Programming implements Phase {

	@Override
	public void work() {
		System.out.println("プログラミング");
	}
}
