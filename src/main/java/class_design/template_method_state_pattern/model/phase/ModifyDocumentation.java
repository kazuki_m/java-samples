package class_design.template_method_state_pattern.model.phase;

import class_design.template_method_state_pattern.base_model.Phase;

public class ModifyDocumentation implements Phase {

	@Override
	public void work() {
		System.out.println("設計書修正");
	}
}
