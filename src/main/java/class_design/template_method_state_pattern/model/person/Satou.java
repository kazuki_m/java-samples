package class_design.template_method_state_pattern.model.person;

import class_design.template_method_state_pattern.base_model.Employee;

public class Satou implements Employee{

	@Override
	public void set() {
		System.out.println("hogehoge");
	}
	@Override
	public void takeTrain() {
		System.out.println("JRを使用");
	}

	@Override
	public void work() {
		System.out.println("設計書作成");
	}
}
