package class_design.template_method_state_pattern.model.person;

import class_design.template_method_state_pattern.base_model.Employee;

public class Takahashi implements Employee {

	@Override
	public void set() {
		System.out.println("朝食を取る");
	}

	@Override
	public void takeTrain() {
		System.out.println("東京メトロ使用");
	}

	@Override
	public void work() {
		System.out.println("製造");
	}
}
