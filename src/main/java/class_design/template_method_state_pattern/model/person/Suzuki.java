package class_design.template_method_state_pattern.model.person;

import class_design.template_method_state_pattern.base_model.Employee;
import class_design.template_method_state_pattern.base_model.Phase;

public class Suzuki implements Employee{
	private Phase phase;

	public Suzuki(Phase phase) {
		this.phase = phase;
	}

	@Override
	public void set() {
		System.out.println("朝風呂に入る");
	}

	@Override
	public void takeTrain() {
		System.out.println("東急を使用");
	}

	// フェーズによって作業内容を変えたい
	// if文で分岐すると可読性が低い、phaseが増えると更にロジックが長くなる、一度完成したプログラムに手を入れることによって、再度テストの必要性が生まれる
	@Override
	public void work() {
		this.phase.work();
	}
}
