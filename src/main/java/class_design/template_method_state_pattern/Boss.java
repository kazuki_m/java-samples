package class_design.template_method_state_pattern;

import class_design.template_method_state_pattern.base_model.Employee;
import class_design.template_method_state_pattern.model.person.Satou;

/**
 *
 * @author kazuki
 * パッケージ、modelを使用するクラス
 * 身支度させ、通勤させ、働かせることが出来るが、何をしているかまでは知らない
 */
public class Boss {

	private final Employee employee;

	// インスタンス化する対象を変えることで、中の実装を知らずに命令を出すことができる
	public Boss() {
		this.employee = new Satou();
		this.employee.work();
	}
}
