package class_design.inner_class;

import class_design.inner_class.Outer.Inner;

// クラス間の結びつきを強調できる
// 継承してサブクラスを定義するまでもないときに使うことが多い
public class Main {

	public static void main(String[] args) {
		Outer.Inner ic = new Inner();
		ic.calculate(5);
	}
}

class Outer {
	private static final int HOGE_NUMBER = 2;

	static class Inner {
		void calculate(int fugaNumber) {
			System.out.println(fugaNumber * HOGE_NUMBER);
		}
	}
}
