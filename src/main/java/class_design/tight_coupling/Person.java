package class_design.tight_coupling;

import java.util.ArrayList;

public class Person {

	private String name;

	private ArrayList<String> interests;

	public Person(String name) {
		this.name = name;
		this.interests = new ArrayList<String>();
	}
	// これまでの関心ごとを全てメソッドの引数で与えられたもので更新する
	void refleshInterests(ArrayList<String> newInterests) {
		this.interests = newInterests;
	}

	// Personの関心ごとをreturnする
	ArrayList<String> getAllInterests(){
		return this.interests;
	}
}
