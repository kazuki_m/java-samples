package class_design.tight_coupling;

import java.util.ArrayList;

// 密結合のPersonクラスを使用する
public class TightCoupling {

	public static void main(String[] args) {

		Person p = new Person("HOGEHOGE FUGA");

		ArrayList<String> newIntersts = new ArrayList<>();
		newIntersts.add("テレビ制作");
		newIntersts.add("野球");
		newIntersts.add("toto BIG");

		p.refleshInterests(newIntersts);

		ArrayList<String> interests = p.getAllInterests();

	}
}
