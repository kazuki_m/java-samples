# java-samples
Javaの練習（クラス設計、デザインパターン）



# Directory structure
```
── main
│   ├── java
│   │   ├── class_design
│   │   │   ├── inner_class
│   │   │   ├── loose_coupling
│   │   │   ├── template_method_state_pattern
│   │   │   │   ├── base_model
│   │   │   │   └── model
│   │   │   │       ├── person
│   │   │   │       └── phase
│   │   │   └── tight_coupling
│   │   └── design_pattern（デザインパターン：2018/4、主にコミット）
│   │       ├── adapter_pattern
│   │       │   ├── delegation
│   │       │   └── inheritance
│   │       ├── builder_pattern
│   │       ├── factory_method_pattern
│   │       │   ├── framework
│   │       │   └── idcard
│   │       ├── iterator_pattern
│   │       ├── singleton_pattern
│   │       └── template_method
│   └── resources
└── test
    ├── java
    └── resources
```
